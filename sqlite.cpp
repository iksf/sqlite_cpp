/*
author: Kieran McKenzie
*/
#include "sqlite.h"

using namespace std;

DBHandle::DBHandle() {
  int error = sqlite3_open(DB_PATH, &this->db);
  if (error != SQLITE_OK) {
    sqlite3_close_v2(this->db);
    free(this->db);
    throw "Failed to open DB";
  }
}

DBHandle::~DBHandle() {
  sqlite3_close_v2(this->db);
  if (this->db != nullptr) {
    free(this->db);
  }
}
QueryResults &DBHandle::dispatch(Query q) {
  return this->dispatch_with(q, callback_fn);
}
QueryResults &DBHandle::dispatch_with(Query q,
                                      int (*callback)(void *, int, char **,
                                                      char **)) {
  char *error;
  int rc =
      sqlite3_exec(this->db, q.get_sql(), callback, q.get_results(), &error);
  if (rc != SQLITE_OK) {
    std::cerr << error << std::endl;
    sqlite3_free(error);
    delete this;
    throw "Some error";
  }
  return *q.get_results();
}
void DBHandle::debug_print(Query q) {
  this->dispatch_with(q, callback_fn_debug_print);
}
Query::Query() { this->results = new QueryResults(); }

Query &Query::with_sql_statement(char *sql) {
  this->sql_query = sql;
  return *this;
}

Query &Query::with_sql_statement(char const *sql) {
  this->sql_query = (char *)malloc(strlen(sql) + 1);
  memcpy(this->sql_query, sql, strlen(sql) + 1);
  return *this;
}

Query &Query::with_results_store(QueryResults *store) {
  this->results = store;
  return *this;
}
char *Query::get_sql() { return this->sql_query; }
QueryResults *Query::get_results() { return this->results; }
int callback_fn(void *data, int argc, char **argv, char **azColName) {
  if (data == nullptr) {
    throw "Unexpected nullptr";
  }
  QueryResults *results = (QueryResults *)data;
  for (int i = 0; i < argc; i++) {
    if (argv[i] != nullptr && azColName[i] != nullptr) {
      results->insert(azColName[i], argv[i]);
    }
  }

  return 0;
}

int callback_fn_debug_print(void *data, int argc, char **argv,
                            char **azColName) {
  for (int i = 0; i < argc; i++) {
    if (argv[i] != nullptr && azColName[i] != nullptr) {
      std::cerr << azColName[i] << " " << argv[i] << std::endl;
    }
  }
  return 0;
}
QueryResults::QueryResults() {

  this->results = std::vector<std::pair<char *, char *>>();
}

QueryResults::~QueryResults() {
  for (std::pair<char *, char *> p : this->results) {
    if (p.first != nullptr)
      free(p.first);
    if (p.second != nullptr)
      free(p.second);
  }
}

void QueryResults::insert(char *column, char *value) {
  char *c = (char *)malloc(strlen(column) + 1);
  char *v = (char *)malloc(strlen(value) + 1);
  strcpy(v, value);
  strcpy(c, column);
  this->results.push_back(std::pair<char *, char *>(c, v));
}

size_t QueryResults::row_count() { return this->results.size(); }
LookupResult QueryResults::where_column_is(std::string s) {
  vector<pair<string, string>> v;
  for (auto p : this->results) {
    string c = string(p.first);
    if (s == c) {
      string r = string(p.second);
      v.push_back(pair<string, string>(c, r));
    }
  }
  return LookupResult(v);
}
LookupResult QueryResults::where_column_is(std::function<bool(std::string)> f) {
  vector<pair<string, string>> v;
  for (auto p : this->results) {
    string c = string(p.first);
    if (f(c)) {
      string r = string(p.second);
      v.push_back(pair<string, string>(c, r));
    }
  }
  return LookupResult(v);
}

LookupResult QueryResults::where(
    std::function<bool(std::pair<std::string, std::string>)> f) {
  vector<pair<string, string>> v;
  for (auto p : this->results) {
    string c = string(p.first);
    string r = string(p.second);
    pair<string, string> p2;
    if (f(p2)) {
      v.push_back(pair<string, string>(p2.first, p2.second));
    }
  }
  return LookupResult(v);
}
LookupResult::LookupResult(std::vector<std::pair<std::string, std::string>> v) {
  this->s = v;
}

std::vector<std::pair<std::string, std::string>> LookupResult::get_pairs() {
  return this->s;
}
std::vector<std::string> LookupResult::get_results() {
  std::vector<string> v;
  for (auto i : this->s) {
    v.push_back(i.second);
  }
  return v;
}
