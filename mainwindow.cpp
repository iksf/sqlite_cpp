#include "mainwindow.h"
#include "sqlite.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  this->db = new DBHandle();
  ui->setupUi(this);
  ui->tableWidget->setColumnCount(4);
  this->db->debug_print(Query().with_sql_statement("SELECT * FROM sqlite_master where type='table';"));
  QueryResults q =
      this->db->dispatch(Query().with_sql_statement("SELECT * FROM Keys;"));
  ui->tableWidget->setRowCount(q.row_count());
  auto ids = q.where_column_is("id").get_results();
  auto names = q.where_column_is("name").get_results();
  auto out_to = q.where_column_is("out_to").get_results();
  auto out_until = q.where_column_is("out_until").get_results();
  for (size_t i = 0; i <ids.size(); i++) {
    ui->tableWidget->setItem(
        i, 0, new QTableWidgetItem(QString::fromStdString(ids[i])));
    ui->tableWidget->setItem(
        i, 1, new QTableWidgetItem(QString::fromStdString(names[i])));
    ui->tableWidget->setItem(
        i, 2, new QTableWidgetItem(QString::fromStdString(out_to[i])));
    ui->tableWidget->setItem(
        i, 3, new QTableWidgetItem(QString::fromStdString(out_until[i])));
  }
}

MainWindow::~MainWindow() {
  delete ui;
  delete this->db;
}
