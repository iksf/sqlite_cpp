/*
author: Kieran McKenzie
*/
#ifndef SQLITE_H
#define SQLITE_H
#include <cstdlib>
#include <cstring>
#include <functional>
#include <iostream>
#include <memory>
#include <sqlite3.h>
#include <vector>
#define DB_PATH "D:\\University Stuff\\HND Year 3\\Software Engineering and Project Management\\Week2\\build-sqlite_qt-Desktop_Qt_5_11_2_MinGW_32bit2-Debug\\debug\\test.db"

class Query;

class LookupResult {
public:
  std::vector<std::pair<std::string, std::string>> get_pairs();
  std::vector<std::string> get_results();
  LookupResult(std::vector<std::pair<std::string, std::string>>);


private:
  std::vector<std::pair<std::string, std::string>> s;
};

class QueryResults {
public:
  void insert(char *, char *);
  std::vector<std::pair<char *, char *>> results;
  std::vector<std::pair<char *, char *>> *operator->() { return &results; }
  LookupResult where_column_is(std::string);
  LookupResult where_column_is(std::function<bool(std::string)>);
  LookupResult where(std::function<bool(std::pair<std::string, std::string>)>);
  size_t row_count();

  QueryResults();
  ~QueryResults();
};
class DBHandle {
private:
  sqlite3 *db;

public:
  DBHandle();
  ~DBHandle();
  QueryResults &dispatch(Query);
  QueryResults &dispatch_with(Query, int (*)(void *, int, char **, char **));
  void debug_print(Query);
};

class Query {
private:
  QueryResults *results;
  char *sql_query;

public:
  Query();
  char *get_sql();
  QueryResults *get_results();
  Query &with_sql_statement(char *);
  Query &with_sql_statement(char const *);
  Query &with_results_store(QueryResults *);
};

int callback_fn(void *, int, char **, char **);
int callback_fn_debug_print(void *, int, char **, char **);

#endif // SQLITE_H
